;;-*- coding: utf-8; lexical-binding: t; byte-compile-dynamic: t; -*-

(defmacro harag-define-keys (@keymap-name @key-cmd-alist)
  "Map `define-key' over a alist @key-cmd-alist.
Example usage:
;; (xah-fly--define-keys
;;  (define-prefix-command 'xah-fly-dot-keymap)
;;  '(
;;    (\"h\" . highlight-symbol-at-point)m
;;    (\".\" . isearch-forward-symbol-at-point)
;;    (\"1\" . hi-lock-find-patterns)
;;    (\"w\" . isearch-forward-word)))
Version 2020-04-18"
  (let (($keymap-name (make-symbol "keymap-name")))
    `(let ((,$keymap-name , @keymap-name))
       ,@(mapcar
          (lambda ($pair)
            `(define-key
               ,$keymap-name
               (kbd (identity ,(car $pair)))
               ,(list 'quote (cdr $pair))))
          (cadr @key-cmd-alist)))))

(defvar emacs-essential-map (make-sparse-keymap) "Keymap for emacs-essential.")

(defun xah-extend-selection ()
  "Select the current word, bracket/quote expression, or expand selection.
Subsequent calls expands the selection.

when there's no selection,
• if cursor is on a any type of bracket (including parenthesis, quotation mark), select whole bracketed thing including bracket
• else, select current word.

when there's a selection, the selection extension behavior is still experimental. But when cursor is on a any type of bracket (parenthesis, quote), it extends selection to outer bracket.

URL `http://ergoemacs.org/emacs/modernization_mark-word.html'
Version 2020-02-04"
  (interactive)
  (if (region-active-p)
      (progn
        (let (($rb (region-beginning)) ($re (region-end)))
          (goto-char $rb)
          (cond
           ((looking-at "\\s(")
            (if (eq (nth 0 (syntax-ppss)) 0)
                (progn
                  ;; (message "left bracket, depth 0.")
                  (end-of-line) ; select current line
                  (set-mark (line-beginning-position)))
              (progn
                ;; (message "left bracket, depth not 0")
                (up-list -1 t t)
                (mark-sexp))))
           ((eq $rb (line-beginning-position))
            (progn
              (goto-char $rb)
              (let (($firstLineEndPos (line-end-position)))
                (cond
                 ((eq $re $firstLineEndPos)
                  (progn
                    ;; (message "exactly 1 line. extend to next whole line.")
                    (forward-line 1)
                    (end-of-line)))
                 ((< $re $firstLineEndPos)
                  (progn
                    ;; (message "less than 1 line. complete the line.")
                    (end-of-line)))
                 ((> $re $firstLineEndPos)
                  (progn
                    ;; (message "beginning of line, but end is greater than 1st end of line")
                    (goto-char $re)
                    (if (eq (point) (line-end-position))
                        (progn
                          ;; (message "exactly multiple lines")
                          (forward-line 1)
                          (end-of-line))
                      (progn
                        ;; (message "multiple lines but end is not eol. make it so")
                        (goto-char $re)
                        (end-of-line)))))
                 (t (error "logic error 42946"))))))
           ((and (> (point) (line-beginning-position)) (<= (point) (line-end-position)))
            (progn
              ;; (message "less than 1 line")
              (end-of-line) ; select current line
              (set-mark (line-beginning-position))))
           (t
            ;; (message "last resort")
            nil))))
    (progn
      (cond
       ((looking-at "\\s(")
        ;; (message "left bracket")
        (mark-sexp)) ; left bracket
       ((looking-at "\\s)")
        ;; (message "right bracket")
        (backward-up-list) (mark-sexp))
       ((looking-at "\\s\"")
        ;; (message "string quote")
					; string quote
        (mark-sexp))

       ((or (looking-back "\\s_" 1) (looking-back "\\sw" 1))
        ;; (message "left is word or symbol")
        (skip-syntax-backward "_w")
        ;; (re-search-backward "^\\(\\sw\\|\\s_\\)" nil t)
        (push-mark)
        (skip-syntax-forward "_w")
        (setq mark-active t))
       ((and (looking-at "\\s ") (looking-back "\\s " 1))
        ;; (message "left and right both space")
        (skip-chars-backward "\\s ") (set-mark (point))
        (skip-chars-forward "\\s "))
       ((and (looking-at "\n") (looking-back "\n" 1))
        ;; (message "left and right both newline")
        (skip-chars-forward "\n")
        (set-mark (point))
        (re-search-forward "\n[ \t]*\n")) ; between blank lines, select next text block
       (t
        ;; (message "just mark sexp")
        (mark-sexp)
        (exchange-point-and-mark))))))

(defun xah-escape-quotes (@begin @end)
  "Replace 「\"」 by 「\\\"」 in current line or text selection.
See also: `xah-unescape-quotes'

URL `http://ergoemacs.org/emacs/elisp_escape_quotes.html'
Version 2017-01-11"
  (interactive
   (if (use-region-p)
       (list (region-beginning) (region-end))
     (list (line-beginning-position) (line-end-position))))
  (save-excursion
    (save-restriction
      (narrow-to-region @begin @end)
      (goto-char (point-min))
      (while (search-forward "\"" nil t)
	(replace-match "\\\"" "FIXEDCASE" "LITERAL")))))

;;?????????????
(defun xah-kill-word ()
  "Like `kill-word', but delete selection first if there's one.
Version 2018-08-31"
  (interactive)
  (when (use-region-p)
    (delete-region (region-beginning) (region-end)))
  (kill-word 1))

;;?????????????
(defun xah-backward-kill-word ()
  "Like `backward-kill-word', but delete selection first if there's one.
Version 2018-08-31"
  (interactive)
  (when (use-region-p)
    (delete-region (region-beginning) (region-end)))
  (backward-kill-word 1))

(defun harag-select-and-copy ()
  (interactive)
  (xah-extend-selection)
  (copy-region-as-kill (region-beginning) (region-end)))

(defun harag-select-and-cut ()
  (interactive)
  (xah-extend-selection)
  (kill-region (region-beginning) (region-end)))

(defun harag-select-and-paste ()
  (interactive)
  (xah-extend-selection)
  (delete-region (region-beginning) (region-end))
  (yank))

(defun xah-select-text-in-quote ()
  "Select text between the nearest left and right delimiters.
Delimiters here includes the following chars: \"`<>(){}[]“”‘’‹›«»「」『』【】〖〗《》〈〉〔〕（）
This command select between any bracket chars, does not consider nesting. For example, if text is

 (a(b)c▮)

 the selected char is “c”, not “a(b)c”.

URL `http://ergoemacs.org/emacs/modernization_mark-word.html'
Version 2020-11-24"
  (interactive)
  (let (
        ($skipChars "^\"`<>(){}[]“”‘’‹›«»「」『』【】〖〗《》〈〉〔〕（）〘〙")
        $p1)
    (skip-chars-backward $skipChars)
    (setq $p1 (point))
    (skip-chars-forward $skipChars)
    (set-mark $p1)))

(defun xah-clean-empty-lines ()
  "Replace repeated blank lines to just 1.
Works on whole buffer or text selection, respects `narrow-to-region'.

URL `http://ergoemacs.org/emacs/elisp_compact_empty_lines.html'
Version 2017-09-22 2020-09-08"
  (interactive)
  (let ($begin $end)
    (if (use-region-p)
        (setq $begin (region-beginning) $end (region-end))
      (setq $begin (point-min) $end (point-max)))
    (save-excursion
      (save-restriction
        (narrow-to-region $begin $end)
        (progn
          (goto-char (point-min))
          (while (re-search-forward "\n\n\n+" nil "move")
            (replace-match "\n\n")))))))

(defun xah-clean-whitespace ()
  "Delete trailing whitespace, and replace repeated blank lines to just 1.
Only space and tab is considered whitespace here.
Works on whole buffer or text selection, respects `narrow-to-region'.

URL `http://ergoemacs.org/emacs/elisp_compact_empty_lines.html'
Version 2017-09-22 2020-09-08"
  (interactive)
  (let ($begin $end)
    (if (use-region-p)
        (setq $begin (region-beginning) $end (region-end))
      (setq $begin (point-min) $end (point-max)))
    (save-excursion
      (save-restriction
        (narrow-to-region $begin $end)
	
        (progn
          (goto-char (point-min))
          (while (re-search-forward "[ \t]+\n" nil "move")
            (replace-match "\n")))
        (progn
          (goto-char (point-min))
          (while (re-search-forward "\n\n\n+" nil "move")
            (replace-match "\n\n")))
        (progn
          (goto-char (point-max))
          (while (equal (char-before) 32) ; char 32 is space
            (delete-char -1)))
	(progn
          (goto-char (point-min))
          (while (re-search-forward "[ \t\n]+)" nil "move")
            (replace-match ")"))))
      
      (message "white space cleaned"))))

(defun harag-buffer-clean ()
  ;;clean-empty-lines for mark because clean-whitespace would break <br />
  (interactive)
 
  (cond ((derived-mode-p 'markdown)
	 (indent-region (point-min) (point-max))
	 (xah-clean-empty-lines))
	;;((derived-mode-p 'yaml-mode)
	;; (indent-region (point-min) (point-max))
	;; (xah-clean-empty-lines))
	((derived-mode-p 'lisp-mode)
	 ;;(setq indent-tabs-mode nil)
	 
	 (indent-region (point-min) (point-max))
	 (xah-clean-whitespace)
	 (untabify (point-min) (point-max)))
	((derived-mode-p 'js-mode)
	 (indent-region (point-min) (point-max))
	 (xah-clean-whitespace)
	 (xah-clean-empty-lines)
	 (untabify (point-min) (point-max))
	 )))

(defun xah-comment-dwim ()
  "Like `comment-dwim', but toggle comment if cursor is not at end of line.

URL `http://ergoemacs.org/emacs/emacs_toggle_comment_by_line.html'
Version 2016-10-25"
  (interactive)
  (if (region-active-p)
      (comment-dwim nil)
    (let (($lbp (line-beginning-position))
          ($lep (line-end-position)))
      (if (eq $lbp $lep)
          (progn
            (comment-dwim nil))
        (if (eq (point) $lep)
            (progn
              (comment-dwim nil))
          (progn
            (comment-or-uncomment-region $lbp $lep)
            (forward-line)))))))

(defvar xah-brackets nil "string of left/right brackets pairs.")
(setq xah-brackets "()[]{}<>＜＞（）［］｛｝⦅⦆〚〛⦃⦄“”‘’‹›«»「」〈〉《》【】〔〕⦗⦘『』〖〗〘〙｢｣⟦⟧⟨⟩⟪⟫⟮⟯⟬⟭⌈⌉⌊⌋⦇⦈⦉⦊❛❜❝❞❨❩❪❫❴❵❬❭❮❯❰❱❲❳〈〉⦑⦒⧼⧽﹙﹚﹛﹜﹝﹞⁽⁾₍₎⦋⦌⦍⦎⦏⦐⁅⁆⸢⸣⸤⸥⟅⟆⦓⦔⦕⦖⸦⸧⸨⸩｟｠")

(defvar xah-left-brackets '( "(" "{" "[" "<" "〔" "【" "〖" "〈" "《" "「" "『" "“" "‘" "‹" "«" "〘")
  "List of left bracket chars.")

(progn
  ;; make xah-left-brackets based on xah-brackets
  (setq xah-left-brackets '())
  (dotimes ($x (- (length xah-brackets) 1))
    (when (= (% $x 2) 0)
      (push (char-to-string (elt xah-brackets $x))
            xah-left-brackets)))
  (setq xah-left-brackets (reverse xah-left-brackets)))

(defvar xah-right-brackets '( ")" "]" "}" ">" "〕" "】" "〗" "〉" "》" "」" "』" "”" "’" "›" "»" "〙")
  "list of right bracket chars.")

(progn
  (setq xah-right-brackets '())
  (dotimes ($x (- (length xah-brackets) 1))
    (when (= (% $x 2) 1)
      (push (char-to-string (elt xah-brackets $x))
            xah-right-brackets)))
  (setq xah-right-brackets (reverse xah-right-brackets)))

(defun xah-backward-left-bracket ()
  "Move cursor to the previous occurrence of left bracket.
The list of brackets to jump to is defined by `xah-left-brackets'.
URL `http://ergoemacs.org/emacs/emacs_navigating_keys_for_brackets.html'
Version 2015-10-01"
  (interactive)
  (re-search-backward (regexp-opt xah-left-brackets) nil t))

(defun xah-forward-right-bracket ()
  "Move cursor to the next occurrence of right bracket.
The list of brackets to jump to is defined by `xah-right-brackets'.
URL `http://ergoemacs.org/emacs/emacs_navigating_keys_for_brackets.html'
Version 2015-10-01"
  (interactive)
  (re-search-forward (regexp-opt xah-right-brackets) nil t))

;;?????????????????
(defun xah-goto-matching-bracket ()
  "Move cursor to the matching bracket.
If cursor is not on a bracket, call `backward-up-list'.
The list of brackets to jump to is defined by `xah-left-brackets' and `xah-right-brackets'.
URL `http://ergoemacs.org/emacs/emacs_navigating_keys_for_brackets.html'
Version 2016-11-22"
  (interactive)
  (if (nth 3 (syntax-ppss))
      (backward-up-list 1 'ESCAPE-STRINGS 'NO-SYNTAX-CROSSING)
    (cond
     ((eq (char-after) ?\") (forward-sexp))
     ((eq (char-before) ?\") (backward-sexp))
     ((looking-at (regexp-opt xah-left-brackets))
      (forward-sexp))
     ((looking-back (regexp-opt xah-right-brackets) (max (- (point) 1) 1))
      (backward-sexp))
     (t (backward-up-list 1 'ESCAPE-STRINGS 'NO-SYNTAX-CROSSING)))))

(defun harag-forward-sexp ()
  "Move cursor to the next occurrence of right bracket.
The list of brackets to jump to is defined by `xah-right-brackets'.
URL `http://ergoemacs.org/emacs/emacs_navigating_keys_for_brackets.html'
Version 2015-10-01"
  (interactive)
  (cond
   ((looking-at (regexp-opt xah-left-brackets))
    (re-search-forward (regexp-opt xah-left-brackets) nil t)
    (re-search-forward (regexp-opt xah-left-brackets) nil t))
   (t (re-search-forward (regexp-opt xah-left-brackets) nil t))))

(defun harag-backward-sexp ()
  "Move cursor to the next occurrence of right bracket.
The list of brackets to jump to is defined by `xah-right-brackets'.
URL `http://ergoemacs.org/emacs/emacs_navigating_keys_for_brackets.html'
Version 2015-10-01"
  (interactive)
  (re-search-backward (regexp-opt xah-left-brackets) nil t))

(defun key-binding-at-point (key)
  (remove nil
	  (mapcar (lambda (keymap) (when (keymapp keymap)
				     (lookup-key keymap key)))
		  (list
		   ;; More likely
		   (get-text-property (point) 'keymap)
		   (mapcar (lambda (overlay)
			     (overlay-get overlay 'keymap))
			   (overlays-at (point)))
		   ;; Less likely
		   (get-text-property (point) 'local-map)
		   (mapcar (lambda (overlay)
			     (overlay-get overlay 'local-map))
			   (overlays-at (point)))))))

(defun previous-minor-binding (key)
  ;;Ignores current key binding!!!
  (car (seq-take-while (lambda (elt)
			 (not (eq (key-binding key) (cdr elt))))
		       (minor-mode-key-binding key))))

(defun locate-previous-key-binding (key)
  (or
   (key-binding-at-point key)
   (local-key-binding key)
   (previous-minor-binding key)
   (global-key-binding key)))

(defun call-fall-back-binding (key)
  (let ((emacs-essential-map nil)
	(binding (locate-previous-key-binding key)))
    (if binding
	(call-interactively binding))))

;;TODO: Should investigate just binding keymap to nil and using  find-key
;;did not work previously but lexical-binding was not set to t explicitly then
;;and that might be why.

;;preferred-fallback saves having to make an expensive call for
;;something that is used a lot.
(defun harag-conditional-right (&optional preferred-fallback)
  (interactive)
  (if (or  (derived-mode-p 'lisp-mode) (derived-mode-p 'lisp-interaction-mode))
      (call-interactively 'harag-forward-sexp)
    (if preferred-fallback
	(call-interactively preferred-fallback)
      (call-fall-back-binding (kbd "<C-right>")))))

;;preferred-fallback saves having to make an expensive call for
;;something that is used a lot.
(defun harag-conditional-left (&optional preferred-fallback)
  (interactive)
  (if (or  (derived-mode-p 'lisp-mode) (derived-mode-p 'lisp-interaction-mode))
      (call-interactively 'harag-backward-sexp)
    (if preferred-fallback
	(call-interactively preferred-fallback)
      (call-fall-back-binding (kbd "<C-left>")))))

(defun harag-preferred-right ()
  (interactive)
  (harag-conditional-right 'right-word))

(defun harag-preferred-left ()
  (interactive)
  (harag-conditional-left #'left-word))

(defun naive-emacs-essential-config ()

  ;;TODO: What about these
  ;;xah-select-text-in-quote ???
  ;;xah-kill-word
  ;;xah-backward-kill-word
  ;;xah-goto-matching-bracket

  (harag-define-keys
   emacs-essential-map
   '(("C-'" . xah-escape-quotes)
     ("C-SPC" . xah-extend-selection)
     ("M-w" . harag-select-and-copy)
     ("C-w" . harag-select-and-cut)
     ("C-y" . harag-select-and-paste)
     ("M-;" . xah-comment-dwim)
     ("<C-right>" . harag-preferred-right)
     ("<C-left>" . harag-preferred-left)))

  ;;Removes extra blank lines and rubbish whites space.
  (add-hook 'before-save-hook 'harag-buffer-clean)

  ;;Custom search navigation
  (harag-define-keys
   isearch-mode-map
   '(("<up>" . isearch-ring-retreat)
     ("<down>" . isearch-ring-advance)
     ("<left>" . isearch-repeat-backward)
     ("<right>" . isearch-repeat-forward)))
  (harag-define-keys
   minibuffer-local-isearch-map
   '(("<left>" . isearch-reverse-exit-minibuffer)
     ("<right>" . isearch-forward-exit-minibuffer))))

(define-minor-mode naive-emacs-mode
  "Toggle naive emacs mode.
Interactively with no argument, this command toggles the mode.
A positive prefix argument enables the mode, any other prefix
argument disables it.  From Lisp, argument omitted or nil enables
the mode, `toggle' toggles the state.

When naive emacs mode is enabled it will use a mix match of xah-fly functions
and its own, to deliver a very opinionated user experience."
  :global t
  ;; The initial value.
  :init-value nil
  ;; The indicator for the mode line.
  :lighter " Naive-Emacs"
  ;; The minor mode bindings.
  :keymap emacs-essential-map)

(define-globalized-minor-mode global-naive-emacs-mode naive-emacs-mode
  (lambda () (naive-emacs-mode)))

(provide 'naive-emacs-mode)
